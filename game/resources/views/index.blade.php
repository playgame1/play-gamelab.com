<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Game Page</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <!-- <link href="{{asset('assets/img/favicon.png')}}" rel="icon">
    <link href="{{asset('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon"> -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('assets/vendor/aos/aos.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
</head>
<body>
<!-- ======= Header ======= -->
<!-- End Header -->

<!-- ======= Gallery Section ======= -->
    <section id="gallery" class="gallery">
   
      <div class="container-fluid" data-aos="fade-up">
        <div class="gallery-slider swiper-container">
          <div class="swiper-wrapper">
            <div class="swiper-slide"><a href="{{asset('assets/img/hero-1.jpg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{asset('assets/img/hero-1.jpg')}}" class="img-fluid" alt=""></a></div>
            <div class="swiper-slide"><a href="{{asset('assets/img/hero-2.jpg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{asset('assets/img/hero-2.jpg')}}" class="img-fluid" alt=""></a></div>
            <div class="swiper-slide"><a href="{{asset('assets/img/hero-3.jpg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{asset('assets/img/hero-3.jpg')}}" class="img-fluid" alt=""></a></div>
            
          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
      <!-- ======= Hero Section ======= -->
        <section id="hero" class="d-flex align-items-center">
            <div class="container">
                <div class="row">
                    
                        <div>
                            <a href="https://m.me/koy2021" class="download-btn">Pay Game</a>
                            <h2>ဒီ Game လေးက မအလက ပိုက်ဆံအတင်း လိုချင်လို့ ငိုပြီးတောင်း‌တဲ့ဂိမ်းလေးပါ ၊ အဲ့တော့ မအလ လက်ထဲပိုက်ဆံ မရောက်အောင်ကာကွှယ်ပါ ။
ဤ ဂိမ်း ကို PLUGIN မှ ရေးသားထားပါသည်။</h2>
                            <a href="https://play.google.com/store/apps/details?id=com.pay.plu" class="download-btn"><i class="bx bxl-play-store"></i> Google Play</a>
                            <a href="https://m.apkpure.com/pay/com.pay.plu" class="download-btn"><i class="bx bxl-apk"></i> APKPure</a>
                        </div>

                </div>
            </div>
        </section><!-- End Hero -->
    </section><!-- End Gallery Section -->

      <!-- Vendor JS Files -->
    <script src="{{asset('assets/vendor/aos/aos.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/vendor/glightbox/js/glightbox.min.js')}}"></script>
    <script src="{{asset('assets/vendor/php-email-form/validate.js')}}"></script>
    <script src="{{asset('assets/vendor/swiper/swiper-bundle.min.js')}}"></script>

    <!-- Template Main JS File -->
    <script src="{{asset('assets/js/main.js')}}"></script>
</body>
</html>