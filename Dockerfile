FROM nginx:1.19.1-alpine

ADD https://packages.whatwedo.ch/php-alpine.rsa.pub /etc/apk/keys/php-alpine.rsa.pub

RUN apk --update add ca-certificates

RUN echo "https://packages.whatwedo.ch/php-alpine/v3.11/php-7.3" >> /etc/apk/repositories


# packages
RUN apk update --update -q && apk add -q --no-cache \
	supervisor \
	tzdata \
	gettext \
	curl \
	zip \
	# php
	php-common=7.3 \
	php \
    php-gd \
    php-bcmath \
    php-json \
    php-ctype \
    php-iconv \
    php-calendar \
    php-zip \
	php-curl \
	php-dom \
	php-fpm \
	php-gettext \
	php-json \
	php-pcntl \
	php-posix \
	php-mbstring \
	php-openssl \
	php-pdo \
    php-pdo_mysql \
    php-mysqlnd \
	php-phar \
	php-opcache \
	php-session \
	php-intl \
	php-xml \
	php-zlib

RUN rm -rf /var/cache/apk/*


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

# set www-data group (82 is the standard uid/gid for www-data in Alpine)
RUN set -x ; \
	addgroup -g 82 -S www-data ; \
	adduser -u 82 -D -S -G www-data www-data && exit 0 ; exit 1

# prepare folders
RUN rm /etc/nginx/conf.d/default.conf && \
	mkdir -p /var/log/supervisord

WORKDIR /var/www
COPY ./game /var/www

RUN composer install --no-scripts --optimize-autoloader --no-dev

RUN cp .env.example .env && php artisan key:generate && php artisan storage:link 
RUN chown -R www-data:www-data /var/www
RUN chmod -R 755 /var/www/storage
RUN chmod -R 755 /var/www/bootstrap/cache

COPY supervisord.conf /etc/supervisord.conf


COPY start /root/start

WORKDIR /var/www

# expose
EXPOSE 80

# entrypoint
ENTRYPOINT ["/root/start"]
